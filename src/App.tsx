import React, { useEffect } from 'react';
import { Routes, Route, Navigate, useNavigate } from 'react-router-dom';
import Documents from './pages/Documents/Documents';
import './App.css'
import Clients from './pages/Clients/Clients';
import Calculation from './pages/Calculation/Calculation';
import Expres from './pages/Calculation/pages/Expres/Expres';
import Avia from './pages/Calculation/pages/Avia/Avia';
import Login from './components/auth/LoginPage/LoginPage';
import DashBoard from './pages/DashBoard/DashBoard';
import { useDispatch, useSelector } from 'react-redux';
import { Dispatch } from 'redux';
import { ActionType } from './Redux/Reducers/AuthReducer/AuthReducer';
import { AppState } from './Redux/Reducers/RootReducer';
import { Actions } from './Redux/Actions/Actions';

function App() {
  const dispatch = useDispatch<Dispatch<ActionType>>()
  const currentUser = useSelector((state: AppState) => state.auth.currentUser)

  const navigate = useNavigate()

  const FetchCurrentUser = async (dispatch: Dispatch<ActionType>) => {
    const response = await fetch('http://localhost:3000/api/auth/currentUser')
    console.log(response.json()) 
    if (response.status === 200) {
      const data = await response.json()
      dispatch({
        type: Actions.FETCH_CURRENT_USER,
        payload: data
      })
      navigate('/')
    } else if (response.status === 401) {
      navigate('/login')
    }
  }

  useEffect(() => {
    FetchCurrentUser(dispatch)
  }, [FetchCurrentUser]);

  return (
    <div className='app'>
      <Routes>
        <Route path='/' element={<DashBoard />}>
          <Route path='/' element={<Documents />} />
          <Route path='/clients' element={<Clients />}/>
          <Route path='/calculation' element={<Calculation />}>
            <Route index element={<Expres />}/>
            <Route path='avia' element={<Avia />}/>
          </Route>
        </Route>
        <Route path='/login' element={<Login />}/>
      </Routes>
    </div>
  );
}

export default App;