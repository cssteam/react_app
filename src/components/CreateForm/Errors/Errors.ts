import * as Yup from 'yup'; 

const required = 'Пожалуйста заполните форму!'

export const Validate = Yup.object().shape({
    marking: Yup.string().trim()
        .required(required),
    codeClient: Yup.string().trim()
        .required(required),
    name: Yup.string().trim()
        .required(required),
    phoneNumberFace: Yup.string().trim()
        .required(required),
    phoneNumber: Yup.string().trim()
        .required(required),
    passport: Yup.string().trim()
        .required(required),
    typeFreight: Yup.string()
        .required('Пожалуйста выберите вид фрахта!'),
    placeDeparture: Yup.string()
        .required('Пожалуйста выберите пункт отправки!'),
    placeDepartureOther: Yup.string().when('placeDeparture', ([placeDeparture], schema) => {
        if (placeDeparture === 'Другой город') {
            return Yup.string().required(required);
        } else {
            return schema
        }
    }),
    dateDeparture: Yup.string()
        .required('Пожалуйста назначте дату отправки!'),
    placeDelivery: Yup.string() 
        .required('Пожалуйста выберите пункт доставки!'),
    placeDeliveryOther: Yup.string().when('placeDelivery', ([placeDelivery], schema) => {
        if (placeDelivery === 'Другой город') {
            return Yup.string().required(required);
        } else {
            return schema
        }
    }), 
    dateDelivery: Yup.string()
        .required('Пожалуйста назначте дату доставки!'),
    market: Yup.string().trim()
        .required(required),
    overloadTK: Yup.string()
        .required(required),
    overloadTKOther: Yup.string().when('overloadTK', ([overloadTK], schema) => {
        if (overloadTK === 'Другой город') {
            return Yup.string().required(required);
        } else {
            return schema
        }
    }),
    overloadCity: Yup.string().trim()
        .required(required),

    nameProduct: Yup.string().trim()
        .required(required),
    typeProduct: Yup.string().trim()
        .required(required),
    fake: Yup.string().trim()
        .required(required),
    countPlace: Yup.number()
        .required(required)
        .min(1, required),
    count: Yup.number()
        .required(required)
        .min(1, required),
    weight: Yup.number()
        .required(required)
        .min(1, required),
    volume: Yup.number()
        .required(required)
        .min(1, required),
    tarif: Yup.number()
        .required(required)
        .min(1, required),
    payCheckProduct2: Yup.number()
        .required(required)
        .min(1, required),
    typePackage: Yup.string()
        .required('Пожалуйста выберите тип упаковки!'),
    package2: Yup.number()
        .required(required)
        .min(1, required),
    deliveryStockChina2: Yup.number()
        .required(required)
        .min(1, required),
    curs2: Yup.number()
        .required(required)
        .min(1, required),
    priceCargo: Yup.number()
        .required(required)
        .min(1, required),
    priceCargoPercent: Yup.number()
        .required(required)
        .min(1, required),
    unloadOverload: Yup.number()
        .required(required)
        .min(1, required),
    unloadOverloadCurs: Yup.number()
        .required(required)
        .min(1, required),  
    expenses: Yup.number()
        .required(required)
        .min(1, required),
    descPay: Yup.string()
        .required(required),
    payBefore: Yup.string()
        .required(required),
    curs3: Yup.number()
        .required(required)
        .min(1, required),
});