import {Formik, Form, Field, ErrorMessage, validateYupSchema, FormikProps} from 'formik';
import './CreateForm.css';
import * as Yup from 'yup'; 
import DateFirst from './components/1_HeadForm/HeadForm';
import DateSecond from './components/2_DateUser/DateUser';
import DateThird from './components/3_DateProduct/DateProduct';
import PayProduct from './components/4_PayProduct/PayProduct';
import Rest from './components/5_Rest/Rest';
import Finish from './components/6_Finish/Finish';
import DateProduct from './components/3_DateProduct/DateProduct';
import DateUser from './components/2_DateUser/DateUser';
import HeadForm from './components/1_HeadForm/HeadForm';
import { initialValues } from './InitialValues/InitialValues';
import { Validate } from './Errors/Errors';
import { MyFormValues } from '../../types/types';

interface ModalProps {
    onClose: () => void;
}

export const Modal = ({ onClose }: ModalProps) => {
    return (
        <Formik
            initialValues={initialValues}
            validationSchema={Validate}
            onSubmit={(values) => {
                alert(JSON.stringify(values, null, 2));
                // console.log(values)
            }}
        >   
            {(formik: FormikProps<MyFormValues>) => {
                return (
                <Form onSubmit={formik.handleSubmit}>
                    <div className='modal_background'>
                        <div className="modal">
                            <HeadForm formik={formik} onClose={onClose}/>
                            <div className="modal_between" />
                            <DateUser formik={formik}/>
                            <div className="modal_between" />
                            <DateProduct formik={formik} />
                            <div className="modal_between" />
                            <PayProduct formik={formik}/>
                            <div className="modal_between" />
                            <Rest formik={formik}/>
                            <div className="modal_between" />
                            <Finish formik={formik}/>
                            <div className="button_create">
                                <button type='submit'>Создать</button>
                            </div>
                        </div>
                    </div>
                    {formik.values.placeDepartureOther}
                </Form>
            )}}
        </Formik>
    )
};

export default Modal;