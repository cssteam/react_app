import { MyFormValues } from "../../../types/types";

export const textArea = '1. Предоставлять четкое описание товара! В противном случае груз не пройдет таможню и будет наложен штраф! \n 2. Внимание: Страховка не покрывает порчу товара! Перед отправкой обязательно обговорить упаковку с клиентом!\n 3.Сроки доставки могут меняться в зависимости от эпидемиологической ситуации. В случае задержки скидки и компенсации не предусмотрены !\n 4. При забора груза в Москве. В случае потери, сразу попросить московский офис (М-офис) выдать справку. Если доказательства нет а груз забрали и ушли то не будет компенсации.\n 5. При перегрузки груза в Москве, если груз целый в Москве фотографирует на месте, компания больше не будет нести ответственность за утерянный товар при транспортировке транзитной компанией. Если клиент не согласен, то может найти кого-нибудь забрать товар в Москве или найти логистическую компанию для перегрузки. Если товар был поврежден, М офис выдаст справку и сразу решит вопрос компенсации. Товар не был поврежден клиент просит взвесить на месте в Москве: 100 руб. /шт. - обычные упаковки, 500 руб. /шт. - погрузчика!'


export const initialValues: MyFormValues =
    {
        marking: '',
        codeClient: '',
        name: '',
        phoneNumberFace: '',
        phoneNumber: '',
        passport: '',
        typeFreight: 'Экспрксс',    
        placeDeparture: 'Пекин',
        placeDepartureOther: '',
        dateDeparture: '',
        placeDelivery: 'Москва',
        placeDeliveryOther: '',
        dateDelivery: '',
        market: '',
        overloadTK: 'Желдор Экспидиция',
        overloadCity: '',

        nameProduct: '',
        typeProduct: '',
        fake: '',
        count: 0,
        weight: 0,
        volume: 0,
        countPlace: 0,
        density: 0,
        tarif: 0,
        generalTarif: 0, 

        payCheckProduct2: 0,
        payCheckProduct1: 0,
        typePackage: 'Дерева',
        package2: 0,
        package1: 0,
        deliveryStockChina2: 0,
        deliveryStockChina1: 0,
        curs2: 0,

        priceCargo: 0,
        priceCargoKg: 0,
        priceCargoPercent: 0,

        totalPriceCargo: 0,

        unloadOverload: 0,
        unloadOverloadCurs: 0,
        totalPriceUnloadOverload: 0,

        expenses: 0,
        descPay: '',

        payBefore: '',
        curs3: 0,

        whatsAppComplaint: '+79199993631',
        description: textArea,

        totalPrice: 0,
    };