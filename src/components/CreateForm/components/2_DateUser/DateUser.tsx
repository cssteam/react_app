import React from 'react';
import {Field, FormikErrors, FormikProps, FormikTouched, FormikValues} from 'formik';
import './DateUser.css'
import { MyFormValues } from '../../../../types/types';

interface DateSecondProps {
    formik: FormikProps<MyFormValues>;
}

const DateUser = ({formik}: DateSecondProps) => {
    return (
        <div className='DateSecond'>
            <div className="modal_second">
                <div className="modal_second_left">
                    <div className="modal_second_left_item">
                        <label>ФИО</label>
                        <Field 
                            type='text'
                            id='name'
                            name='name'
                            className={ formik.errors.name && formik.touched.name && 'input-error' }
                        />
                        {
                            formik.errors.name && formik.touched.name && 
                            <div className='error'>{ formik.errors.name }</div>
                        }
                    </div>
                    <div className="modal_second_left_item">
                        <label>Номер телефона ответственного лица</label>
                        <Field 
                            type='text'
                            id='phoneNumberFace'
                            name='phoneNumberFace'
                            className={ formik.errors.phoneNumberFace && formik.touched.phoneNumberFace && 'input-error' }
                        />
                        {
                            formik.errors.phoneNumberFace && formik.touched.phoneNumberFace && 
                            <div className='error'>{ formik.errors.phoneNumberFace }</div>
                        }
                    </div>
                    <div className="modal_second_left_item">
                        <label>Номер телефона</label>
                        <Field 
                            type='text'
                            id='phoneNumber'
                            name='phoneNumber'
                            className={ formik.errors.phoneNumber && formik.touched.phoneNumber && 'input-error' }
                        />
                        {
                            formik.errors.phoneNumber && formik.touched.phoneNumber && 
                            <div className='error'>{ formik.errors.phoneNumber }</div>
                        }
                    </div>
                    <div className="modal_second_left_item">
                        <label>Паспорт</label>
                        <Field 
                            type='text'
                            id='passport'
                            name='passport'
                            className={ formik.errors.passport && formik.touched.passport && 'input-error' }
                        />
                        {
                            formik.errors.passport && formik.touched.passport && 
                            <div className='error'>{ formik.errors.passport }</div>
                        }
                    </div>
                    <div className="modal_second_left_item">
                        <label>Вид фрахта</label>
                        <Field 
                            component='select'
                            id='typeFreight'
                            name='typeFreight'
                            className={ formik.errors.typeFreight && formik.touched.typeFreight && 'input-error' }
                        >
                            <option value="Экспресс">Экспресс</option>
                            <option value="Авиа">Авиа</option>
                            <option value="Медленная Авиа">Медленная Авиа</option>
                            <option value="Быстрый авто">Быстрый авто</option>
                            <option value="Быстрый авто (по белой)">Быстрый авто (по белой)</option>
                            <option value="Медленный Авто">Медленный Авто</option>
                            <option value="Поезд">Поезд</option>
                            <option value="Поезд (Один пояс один путь)">Поезд (Один пояс один путь)</option>
                            <option value="Быстрый авто (Одежда)">Быстрый авто (Одежда)</option>
                            <option value="Медленный авто (Одежда)">Медленный авто (Одежда)</option>
                            <option value="Поезд (Одежда)">Поезд (Одежда)</option>
                        </Field>
                        {
                            formik.errors.typeFreight && formik.touched.typeFreight && 
                            <div className='error'>{ formik.errors.typeFreight }</div>
                        }
                    </div>
                </div>
                <div className="modal_second_right">
                    <div className="modal_second_right_item">
                        <label>Пункт отправки</label>
                        <Field 
                            component='select'
                            id='placeDeparture' 
                            name='placeDeparture'
                            className={ formik.errors.placeDeparture && formik.touched.placeDeparture && 'input-error' }
                        >
                            <option value="Пекин">Пекин</option>
                            <option value="Гуанчжоу">Гуанчжоу</option>
                            <option value="Иу">Иу</option>
                            <option value="Урумчи">Урумчи</option>
                            <option value="Другой город">Другой город</option>
                        </Field>
                        {
                            formik.values.placeDeparture === 'Другой город' && 
                            <div>
                                <Field 
                                    type='text'
                                    id='placeDepartureOther'
                                    name='placeDepartureOther'
                                    placeholder='Введите другой город'
                                    className={ formik.errors.placeDepartureOther && formik.touched.placeDepartureOther ? 'input-error mt-5' : 'mt-5' }
                                />
                                {
                                    formik.errors.placeDepartureOther && formik.touched.placeDepartureOther && 
                                    <div className='error'>{ formik.errors.placeDepartureOther }</div>
                                }
                            </div>
                        }
                        {
                            formik.errors.placeDeparture && formik.touched.placeDeparture &&
                            <div className='error'>{ formik.errors.placeDeparture }</div>
                        }
                    </div>
                    <div className="modal_second_right_item">
                        <label>Дата отправки</label>
                        <Field
                            type='date'
                            id='dateDeparture'
                            name='dateDeparture'
                            className={ formik.errors.dateDeparture && formik.touched.dateDeparture && 'input-error' }
                        />
                        {
                            formik.errors.dateDeparture && formik.touched.dateDeparture &&
                            <div className='error'>{ formik.errors.dateDeparture }</div>
                        }
                    </div>
                    <div className="modal_second_right_item">
                        <label>Пункт доставки</label>
                        <Field 
                            component='select'
                            id='placeDelivery'
                            name='placeDelivery'
                            className= {formik.errors.placeDelivery && formik.touched.placeDelivery && 'input-error' }
                        >
                            <option value="Москва">Москва</option>
                            <option value="Другой город">Другой город</option>
                        </Field>
                        {
                            formik.values.placeDelivery === 'Другой город' &&
                            <div>
                                <Field 
                                    type='text'
                                    id='placeDeliveryOther'
                                    name='placeDeliveryOther'
                                    placeholder='Введите другой город'
                                    className={ formik.errors.placeDeliveryOther && formik.touched.placeDeliveryOther ? 'input-error mt-5' : 'mt-5' }
                                />
                                {
                                    formik.errors.placeDeliveryOther && formik.touched.placeDeliveryOther &&
                                    <div className='error'>{ formik.errors.placeDeliveryOther }</div>
                                }
                            </div>
                        }
                        {
                            formik.errors.placeDelivery && formik.touched.placeDelivery &&
                            <div className='error'>{ formik.errors.placeDelivery }</div>
                        }
                    </div>
                    <div className="modal_second_right_item">
                        <label>Дата доставки</label>
                        <Field 
                            type='date'
                            id='dateDelivery'
                            name='dateDelivery'
                            className={ formik.errors.dateDelivery && formik.touched.dateDelivery && 'input-error' }
                        />
                        {
                            formik.errors.dateDelivery && formik.touched.dateDelivery &&
                            <div className='error'>{ formik.errors.dateDelivery }</div>
                        }
                    </div>
                    <div className="modal_second_right_item">
                        <label>Рынок</label>
                        <Field 
                            type='text'
                            id='market'
                            name='market'
                            placeholder='Рынок'
                            className={ formik.errors.market && formik.touched.market && 'input-error' }
                        />
                        {
                            formik.errors.market && formik.touched.market &&
                            <div className='error'>{ formik.errors.market }</div>
                        }
                    </div>
                    <div className="modal_second_right_item">
                        <label>Перезагрузка на TK</label>
                        <Field 
                            component='select'
                            id='overloadTK'
                            name='overloadTK'
                            className={ formik.errors.overloadTK && formik.touched.overloadTK && 'input-error' }
                        >
                            <option value="Желдор Экспидиция">Желдор Экспидиция</option>
                            <option value="Байкал Сервис">Байкал Сервис</option>
                            <option value="Деловые Линии">Деловые Линии</option>
                            <option value="Энергия">Энергия</option>
                            <option value="СДЭК">СДЭК</option>
                            <option value="Кит">Кит</option>
                            <option value="Другой город">Другой город</option>
                        </Field>
                        {
                            formik.values.overloadTK === 'Другой город' &&
                            <div>
                                <Field
                                    type='text'
                                    id='overloadTKOther'
                                    name='overloadTKOther'
                                    placeholder='Введите другой город'
                                    className={ formik.errors.overloadTKOther && formik.touched.overloadTKOther ? 'input-error mt-5' : 'mt-5' }
                                />
                                {
                                    formik.errors.overloadTKOther && formik.touched.overloadTKOther &&
                                    <div className='error'>{ formik.errors.overloadTKOther }</div>
                                }
                            </div>
                        }
                        {
                            formik.errors.overloadTK && formik.touched.overloadTK &&
                            <div className='error'>{ formik.errors.overloadTK }</div>
                        }
                    </div>
                    <div className="modal_second_right_item">
                        <label>Перегрузка до города</label>
                        <Field 
                            type='text'
                            id='overloadCity'
                            name='overloadCity'
                            className={ formik.errors.overloadCity && formik.touched.overloadCity && 'input-error' }
                        />
                        {
                            formik.errors.overloadCity && formik.touched.overloadCity &&
                            <div className='error'>{ formik.errors.overloadCity }</div>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DateUser;