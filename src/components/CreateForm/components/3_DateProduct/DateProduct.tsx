import React, { useState } from 'react';
import {ErrorMessage, Field, FormikErrors, FormikProps, FormikTouched, FormikValues} from 'formik';
import './DateProduct.css'
import { MyFormValues } from '../../../../types/types';

interface DateThirdProps {
    formik: FormikProps<MyFormValues>;
}

const DateProduct = ({formik}: DateThirdProps) => {

    const Density = () => {
        if (formik.values.weight === 0 || formik.values.volume === 0) {
            return 0
        } else if (String(formik.values.weight).trim() === '' || String(formik.values.volume).trim() === '') {
            return 0
        } else {
            return Math.round(Number(formik.values.weight) / Number(formik.values.volume))
        }
        
    }

    const GeneralTarif = () => {
        return Number(formik.values.volume) * Number(formik.values.tarif)
    }

    return (
        <div className='DateThird'>
            <div className="modal_third">
                <div className="modal_third_row_1">
                    <div className="modal_third_row_1_left">
                        <div className="modal_third_row_1_left_item">
                            <label>Наименование товара</label>
                            <Field 
                                type='text'
                                id='nameProduct'
                                name='nameProduct'
                                className={ formik.errors.nameProduct && formik.touched.nameProduct && 'input-error'}
                            />
                            {
                                formik.errors.nameProduct && formik.touched.nameProduct &&
                                <div className='error'>{ formik.errors.nameProduct }</div>
                            }
                        </div>
                        <div className="modal_third_row_1_left_item">
                            <label>Тип товара</label>
                            <Field 
                                type='text'
                                id='typeProduct'
                                name='typeProduct'
                                className={ formik.errors.typeProduct && formik.touched.typeProduct && 'input-error'}
                            />
                            {
                                formik.errors.typeProduct && formik.touched.typeProduct &&
                                <div className='error'>{ formik.errors.typeProduct }</div>
                            }
                        </div>
                        <div className="modal_third_row_1_left_item">
                            <label>Подделка</label>
                            <Field 
                                type='text'
                                id='fake'
                                name='fake'
                                className={ formik.errors.fake && formik.touched.fake && 'input-error'}
                            />
                            {
                                formik.errors.fake && formik.touched.fake &&
                                <div className='error'>{ formik.errors.fake }</div>
                            }
                        </div>
                        <div className="modal_third_row_1_left_item">
                            <label>К-во мест</label>
                            <Field 
                                type='number'
                                id='countPlace'
                                name='countPlace'
                                className={ formik.errors.countPlace && formik.touched.countPlace && 'input-error' }
                            />
                            {
                                formik.errors.countPlace && formik.touched.countPlace && 
                                <div className='error'>{ formik.errors.countPlace }</div>
                            }
                        </div>
                    </div>
                    <div className="modal_third_row_1_right">
                        <div className="modal_third_row_1_right_item">
                            <label>Количество</label>
                            <Field 
                                type='number'
                                id='count'
                                name='count'
                                className={ formik.errors.count && formik.touched.count && 'input-error' }
                            />
                            {
                                formik.errors.count && formik.touched.count && 
                                <div className='error'>{ formik.errors.count }</div>
                            }
                        </div>
                        <div className="modal_third_row_1_right_item">
                            <label>Вес(кг)</label>
                            <Field
                                type='number'
                                id='weight'
                                name='weight'
                                className={ formik.errors.weight && formik.touched.weight && 'input-error' }
                            />
                            {
                                formik.errors.weight && formik.touched.weight && 
                                <div className='error'>{ formik.errors.weight }</div>
                            }
                        </div>
                        <div className="modal_third_row_1_right_item">
                            <label>Обьем(м3)</label>
                            <Field 
                                type='number'
                                id='volume'
                                name='volume'
                                className={ formik.errors.volume && formik.touched.volume && 'input-error' }
                            />
                            {
                                formik.errors.volume && formik.touched.volume && 
                                <div className='error'>{ formik.errors.volume }</div>
                            }
                        </div>
                    </div>
                </div>
                <div className="modal_third_row_2">
                    <div className="modal_third_row_2_item">
                        <label>Плотность</label>
                        <Field
                            type='number'
                            id='density'
                            name='density'
                            value={Density()}
                            // Validate={Density}
                            // value={Math.round(values.weight / values.volume)}
                            disabled
                            className='disabled'
                        />
                    </div>
                    <div className="modal_third_row_2_item">
                        <label>Тариф/кг ( $ )</label>
                        <Field 
                            type='number'
                            id='tarif'
                            name='tarif'
                            className={ formik.errors.tarif && formik.touched.tarif && 'input-error' }
                        />
                        {
                            formik.errors.tarif && formik.touched.tarif && 
                            <div className='error'>{ formik.errors.tarif }</div>
                        }
                    </div>
                    <div className="modal_third_row_2_item">
                        <label>Общий тариф</label>
                        <Field 
                            type='number'
                            id='generalTarif'
                            name='generalTarif'
                            disabled
                            value={GeneralTarif()}
                            className='disabled'
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DateProduct;