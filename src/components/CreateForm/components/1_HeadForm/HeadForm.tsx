import React from 'react';
import {Field, FormikErrors, FormikProps, FormikTouched} from 'formik';
import './HeadForm.css'
import { MyFormValues } from '../../../../types/types';

interface DateFirstProps {
    onClose?: () => void;
    formik: FormikProps<MyFormValues>;
}

const DateForm = ({ onClose, formik }: DateFirstProps) => {
    return (
        <div className="DateFirst">
            <div className="modal_first">
                <div className="modal_first_row_1">
                    <div className="modal_first_row_1_item">
                        <div className="modal_first_row_1_item_button">
                            <button onClick={onClose}>назад</button>
                        </div>
                    </div>
                    <div className="modal_first_row_1_item">
                    
                    </div>
                </div>
                <div className="modal_first_row_2">
                    <div className="modal_first_row_2_item">
                        <div className="modal_first_row_2_item_text">
                            Создание накладной
                        </div>
                    </div>
                    <div className="modal_first_row_2_item">
                        <div className="modal_first_row_2_item_language">
                            <select>
                                <option>Русский</option>
                                <option>Китайский</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div className="modal_first_row_3">
                    <div className="modal_first_row_3_item">
                        <label htmlFor="marking">Маркировка</label>
                        <Field 
                            type='text'
                            id='marking'
                            name='marking'
                            value={formik.values.marking}
                            className={ formik.errors.marking && formik.touched.marking && 'input-error' }
                        />
                        {
                            formik.errors.marking && formik.touched.marking &&
                            <div className='error'>{ formik.errors.marking }</div>
                        }
                    </div>
                    <div className="modal_first_row_3_item">
                        <label htmlFor="marking">Код клиента</label>
                        <Field 
                            type='text'
                            id='codeClient'
                            name='codeClient'
                            className={ formik.errors.codeClient && formik.touched.codeClient && 'input-error' }
                        />
                        {
                            formik.errors.codeClient && formik.touched.codeClient &&
                            <div className='error'>{ formik.errors.codeClient }</div>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DateForm;