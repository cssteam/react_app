import {Field, FormikErrors, FormikProps, FormikTouched, FormikValues, isInteger} from 'formik';
import React from 'react';
import './Rest.css'
import { MyFormValues } from '../../../../types/types';

interface RestProps {
    formik: FormikProps<MyFormValues>;
}

const Rest = ({ formik }: RestProps) => {

    const PriceCargoKg = () => {
        let let_1 = Number(formik.values.priceCargo) / Number(formik.values.weight)
        if (formik.values.priceCargo === 0 || formik.values.weight === 0) {
            formik.values.priceCargoKg = 0
        } else if (String(formik.values.priceCargo).trim() === '' || String(formik.values.weight).trim() === '') {
            formik.values.priceCargoKg = 0
        } else {
            if (let_1 % 1 === 0) {
                formik.values.priceCargoKg = let_1
            } else if (let_1.toFixed(2).slice(-1, ) === '0') {
                formik.values.priceCargoKg = Number(let_1.toFixed(1))
            } else if (let_1.toFixed(2).slice(-2, ) === '00') {
                formik.values.priceCargoKg = Number(let_1.toFixed(0))
            } else {
                formik.values.priceCargoKg= Number(let_1.toFixed(2))
            }
        }
        return formik.values.priceCargoKg
    }

    const totalPriceCargo = () => {
        let let_1 = (Number(formik.values.priceCargo) / 100) * Number(formik.values.priceCargoPercent)
        if (let_1 * Number(formik.values.priceCargoPercent) === 0) {
            formik.values.totalPriceCargo = 0
        } else if (String(formik.values.priceCargo).trim() === '' || String(formik.values.priceCargoPercent).trim() === '') {
            formik.values.totalPriceCargo = 0
        } else {
            if (let_1 % 1 === 0) {
                formik.values.totalPriceCargo = let_1
            } else if (let_1.toFixed(2).slice(-1, ) === '0') {
                formik.values.totalPriceCargo = Number(let_1.toFixed(1))
            } else if (let_1.toFixed(2).slice(-2, ) === '00') {
                formik.values.totalPriceCargo = Number(let_1.toFixed(0))
            } else {
                formik.values.totalPriceCargo = Number(let_1.toFixed(2))
            }
        }
        return formik.values.totalPriceCargo
    }

    const TotalPriceUnloadOverload = () => {
        let let_2 = Number(formik.values.unloadOverload) / Number(formik.values.unloadOverloadCurs) * Number(formik.values.countPlace)
        if (formik.values.unloadOverload === 0 || formik.values.unloadOverloadCurs === 0 || formik.values.countPlace === 0) {
            formik.values.totalPriceUnloadOverload = 0
        } else if (String(formik.values.unloadOverload).trim() === '' || String(formik.values.unloadOverloadCurs).trim() === '' || String(formik.values.countPlace).trim() === '') {
            formik.values.totalPriceUnloadOverload = 0
        } else {
            if (let_2 % 1 === 0) {
                formik.values.totalPriceUnloadOverload = let_2
            } else if (let_2.toFixed(2).slice(-1, ) === '0') {
                formik.values.totalPriceUnloadOverload = Number(let_2.toFixed(1))
            } else if (let_2.toFixed(2).slice(-2, ) === '00') {
                formik.values.totalPriceUnloadOverload = Number(let_2.toFixed(0))
            } else {
                formik.values.totalPriceUnloadOverload = Number(let_2.toFixed(2))
            }
        }
        return formik.values.totalPriceUnloadOverload
    }

    return (
        <div className='Rest'>
            <div className="modal_fifth">
                <div className="modal_fifth_row_1">
                    <div className="modal_fifth_row_1_item">
                        <label>Стоимость груза ( $ )</label>
                        <Field 
                            type='number'
                            id='priceCargo'
                            name='priceCargo'
                            className={ formik.errors.priceCargo && formik.touched.priceCargo && 'input-error' }
                        />
                        {
                            formik.errors.priceCargo && formik.touched.priceCargo &&
                            <div className='error'>{ formik.errors.priceCargo }</div>
                        }
                    </div>
                    <div className="modal_fifth_row_1_item">
                        <label>Стоимость груза за (кг) ( $ )</label>
                        <Field 
                            type='number'
                            id='priceCargoKg'
                            name='priceCargoKg'
                            disabled
                            value={PriceCargoKg()}
                            className='disabled'
                        />
                    </div>
                    <div className="modal_fifth_row_1_item">
                        <label>Стоимость груза в процентах %</label>
                        <Field 
                            type='number'
                            id='priceCargoPercent'
                            name='priceCargoPercent'
                            className={ formik.errors.priceCargoPercent && formik.touched.priceCargoPercent && 'input-error' }
                        />
                        {
                            formik.errors.priceCargoPercent && formik.touched.priceCargoPercent &&
                            <div className='error'>{ formik.errors.priceCargoPercent }</div>
                        }
                    </div>
                </div>
                <div className="modal_between" /> 
                <div className="modal_fifth_row_2">
                    <div className="modal_fifth_row_2_item">
                        <label>Общая стоимость груза ( $ )</label>
                        <Field 
                            type='number'
                            id='totalPriceCargo'
                            name='totalPriceCargo'
                            disabled
                            value={totalPriceCargo()}
                            className='disabled'
                        />
                    </div>
                </div>
                <div className="modal_between" /> 
                <div className="modal_fifth_row_3">
                    <div className="modal_fifth_row_3_item">
                        <label>Разгрузка/Перегрузка ( ₽ )</label>
                        <Field 
                            type='number'
                            id='unloadOverload'
                            name='unloadOverload'
                            className={ formik.errors.unloadOverload && formik.touched.unloadOverload && 'input-error' }
                        />
                        {
                            formik.errors.unloadOverload && formik.touched.unloadOverload &&
                            <div className='error'>{ formik.errors.unloadOverload }</div>
                        }
                    </div>
                    <div className="modal_fifth_row_3_item">
                        <label>Разгрузка/Перегрузка курс ( ₽ )</label>
                        <Field 
                            type='number'
                            id='unloadOverloadCurs'
                            name='unloadOverloadCurs'
                            className={ formik.errors.unloadOverloadCurs && formik.touched.unloadOverloadCurs && 'input-error' }
                        />
                        {
                            formik.errors.unloadOverloadCurs && formik.touched.unloadOverloadCurs &&
                            <div className='error'>{ formik.errors.unloadOverloadCurs }</div>
                        }
                    </div>
                    <div className="modal_fifth_row_3_item">
                        <label>Общая стоимоть разгрузки/перегрузки ( $ )</label>
                        <Field 
                            type='number'
                            id='totalPriceUnloadOverload'
                            name='totalPriceUnloadOverload'
                            disabled
                            value={TotalPriceUnloadOverload()}
                            className='disabled'
                        />
                    </div>
                </div>
                <div className="modal_between" />
                <div className="modal_fifth_row_4">
                    <div className="modal_fifth_row_4_item">
                        <label>Прочие расходы ( $ )</label>
                        <Field 
                            type='number'
                            id='expenses'
                            name='expenses'
                            className={ formik.errors.expenses && formik.touched.expenses && 'input-error' }
                        />
                        {
                            formik.errors.expenses && formik.touched.expenses && 
                            <div className='error'>{ formik.errors.expenses }</div>
                        }
                    </div>
                    <div className="modal_fifth_row_4_item">
                        <label>Описание прочих платежей</label>
                        <Field 
                            type='string'
                            id='descPay'
                            name='descPay'
                            className={ formik.errors.descPay && formik.touched.descPay && 'input-error' }
                        />
                        {
                            formik.errors.descPay && formik.touched.descPay && 
                            <div className='error'>{ formik.errors.descPay }</div>
                        }
                    </div>
                </div>
                <div className="modal_between"/>
                <div className="modal_fifth_row_5">
                    <div className="modal_fifth_row_5_item">
                        <label>Оплата до</label>
                        <Field 
                            type='date'
                            id='payBefore'
                            name='payBefore'
                            className={ formik.errors.payBefore && formik.touched.payBefore && 'input-error' }
                        />
                        {
                            formik.errors.payBefore && formik.touched.payBefore &&
                            <div className='error'>{ formik.errors.payBefore }</div>
                        }
                    </div>
                    <div className="modal_fifth_row_5_item">
                        <label>Курс ¥</label>
                        <Field 
                            type='number'
                            id='curs3'
                            name='curs3'
                            className={ formik.errors.curs3 && formik.touched.curs3 && 'input-error' }
                        />
                        {
                            formik.errors.curs3 && formik.touched.curs3 &&
                            <div className='error'>{ formik.errors.curs3 }</div>
                        }
                    </div>
                </div>
                <div className="modal_between" />
                <div className="modal_fifth_row_6">
                    <div className="modal_fifth_row_6_item">
                        <label>WhatsApp для жалоб</label>
                        <Field 
                            type='text'
                            id='whatsAppComplaint'
                            name='whatsAppComplaint'
                            className='whatsAppComplaint'
                        />
                    </div>
                    <div className="modal_fifth_row_6_item">
                        <label>Заключене</label>
                        <Field 
                            type='text'
                            id='finish'
                            name='finish'
                            className='finish'
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Rest;