import {Field, FormikErrors, FormikProps, FormikTouched, FormikValues} from 'formik';
import React from 'react';
import './Finish.css';
import { MyFormValues } from '../../../../types/types';

interface FinishProps {
    formik: FormikProps<MyFormValues>;
}

const Finish = ({ formik }: FinishProps) => {

    const TotalPrice = () => {
        let result = (
            Number(formik.values.payCheckProduct1) 
            + Number(formik.values.package1) 
            + Number(formik.values.deliveryStockChina1)
            + Number(formik.values.totalPriceCargo) 
            + Number(formik.values.totalPriceUnloadOverload) 
            )
            - Number(formik.values.expenses)
        return Math.round(result)
    };

    const onChangeTextArea = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
        formik.setFieldValue('description', event.target.value);
    }

    return (
        <div className='Finish'>
            <div className="modal_six">
                <div className="modal_six_item">
                    <label>Примечание</label>
                    <textarea 
                        name='description'
                        onChange={onChangeTextArea} 
                        className="modal_six_item_note" 
                        value={formik.values.description}
                    />
                </div>
                <div className="modal_six_item">
                    <label>Итого к оплате ( $ )</label>
                    <Field 
                        type='number'
                        id='totalPrice'
                        name='totalPrice'
                        disabled
                        value={TotalPrice()}
                        className='disabled'
                    />
                </div>
            </div>
        </div>
    );
};

export default Finish;