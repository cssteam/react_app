import React from 'react';
import {Field, FormikErrors, FormikProps, FormikTouched, FormikValues, isInteger} from 'formik';
import './PayProduct.css'
import { MyFormValues } from '../../../../types/types';

interface PayProductProps {
    formik: FormikProps<MyFormValues>;
}

const PayProduct = ({ formik }: PayProductProps) => {

    const payCheckProduct1Func = () => {
        let let_1 = Number(formik.values.payCheckProduct2) / Number(formik.values.curs2)
        if (formik.values.payCheckProduct2 === 0 || formik.values.curs2 === 0) {
            formik.values.payCheckProduct1 = 0
        } else if (String(formik.values.payCheckProduct2).trim() === '' || String(formik.values.curs2).trim() === '') {
            formik.values.payCheckProduct1 = 0
        } else {
            if (let_1 % 1 === 0) {
                formik.values.payCheckProduct1 = let_1
            } else if (let_1.toFixed(2).slice(-1, ) === '0') {
                formik.values.payCheckProduct1 = Number(let_1.toFixed(1))
            } else if (let_1.toFixed(2).slice(-2, ) === '00') {
                formik.values.payCheckProduct1 = Number(let_1.toFixed(0))
            } else {
                formik.values.payCheckProduct1 = Number(let_1.toFixed(2))
            }
        }
        return formik.values.payCheckProduct1
    }

    const package1Func = () => {
        let let_1 = Number(formik.values.package2) / Number(formik.values.curs2)
        if (formik.values.package2 === 0 || formik.values.curs2 === 0) {
            formik.values.package1 = 0
        } else if (String(formik.values.package2).trim() === '' || String(formik.values.curs2).trim() === '') {
            formik.values.package1 = 0
        } else {
            if (let_1 % 1 === 0) {
                formik.values.package1 = let_1
            } else if (let_1.toFixed(2).slice(-1, ) === '0') {
                formik.values.package1 = Number(let_1.toFixed(1))
            } else if (let_1.toFixed(2).slice(-2, ) === '00') {
                formik.values.package1 = Number(let_1.toFixed(0))
            } else {
                formik.values.package1 = Number(let_1.toFixed(2))
            }
        }
        return formik.values.package1
    }

    const deliveryStockChina1Func = () => {
        let let_1 = Number(formik.values.deliveryStockChina2) / Number(formik.values.curs2)
        if (formik.values.deliveryStockChina2 === 0 || formik.values.curs2 === 0) {
            formik.values.deliveryStockChina1 = 0
        } else if (String(formik.values.deliveryStockChina2).trim() === '' || String(formik.values.curs2).trim() === '') {
            formik.values.deliveryStockChina1 = 0
        } else {
            if (let_1 % 1 === 0) {
                formik.values.deliveryStockChina1 = let_1
            } else if (let_1.toFixed(2).slice(-1, ) === '0') {
                formik.values.deliveryStockChina1 = Number(let_1.toFixed(1))
            } else if (let_1.toFixed(2).slice(-2, ) === '00') {
                formik.values.deliveryStockChina1 = Number(let_1.toFixed(0))
            } else {
                formik.values.deliveryStockChina1 = Number(let_1.toFixed(2))
            }
        }
        return formik.values.deliveryStockChina1
    }

    return (
        <div className='PayProduct'>
            <div className="modal_fourth">
                <div className="modal_fourth_left">
                    <div className="modal_fourth_left_item">
                        <label>Плата за проверку товара ( ¥ )</label>
                        <Field 
                            type='number'
                            id='payCheckProduct2'
                            name='payCheckProduct2'
                            className={ formik.errors.payCheckProduct2 && formik.touched.payCheckProduct2 && 'input-error' }
                        />
                        {
                            formik.errors.payCheckProduct2 && formik.touched.payCheckProduct2 && 
                            <div className='error'>{ formik.errors.payCheckProduct2 }</div>
                        }
                    </div>
                    <div className="modal_fourth_left_item">
                        <label>Плата за проверку товара ( $ )</label>
                        <Field 
                            type='number'
                            id='payCheckProduct1'
                            name='payCheckProduct1'
                            disabled
                            value={payCheckProduct1Func()}
                            className='disabled'
                        />
                    </div>
                    <div className="modal_fourth_left_item">
                        <label>Тип Упаковки</label>
                        <Field 
                            component='select'
                            id='typePackage'
                            name='typePackage'
                            className={ formik.errors.typePackage && formik.touched.typePackage && 'input-error' }
                        >
                            <option value="Дерева">Дерева</option>
                            <option value="Обычная упаковка">Обычная упаковка</option>
                            <option value="Палет">Палет</option>
                            <option value="Обрешетка">Обрешетка</option>
                            <option value="Упаковка из пленки">Упаковка из пленки</option>
                        </Field>
                        {
                            formik.errors.typePackage && formik.touched.typePackage &&
                            <div className='error'>{ formik.errors.typePackage }</div>
                        }
                    </div>
                    <div className="modal_fourth_left_item">
                        <label>Упаковка ( ¥ )</label>
                        <Field 
                            type='number'
                            id='package2'
                            name='package2'
                            className={ formik.errors.package2 && formik.touched.package2 && 'input-error' }
                        />
                        {
                            formik.errors.package2 && formik.touched.package2 &&
                            <div className='error'>{ formik.errors.package2 }</div>
                        }
                    </div>
                    <div className="modal_fourth_left_item">
                        <label>Упаковка ( $ )</label>
                        <Field 
                            type='number'
                            id='package1'
                            name='package1'
                            disabled
                            value={package1Func()}
                            className='disabled'
                        />
                    </div>
                </div>
                <div className="modal_fourth_right">
                    <div className="modal_fourth_right_item">
                        <label>Доставка на наш склад в Китае ( ¥ )</label>
                        <Field 
                            type='number'
                            id='deliveryStockChina2'
                            name='deliveryStockChina2'
                            className={ formik.errors.deliveryStockChina2 && formik.touched.deliveryStockChina2 && 'input-error' }
                        />
                        {
                            formik.errors.deliveryStockChina2 && formik.touched.deliveryStockChina2 &&
                            <div className='error'>{ formik.errors.deliveryStockChina2 }</div>
                        }
                    </div>
                    <div className="modal_fourth_right_item">
                        <label>Доставка на наш склад в Китае ( $ )</label>
                        <Field 
                            type='number'
                            id='deliveryStockChina1'
                            name='deliveryStockChina1'
                            disabled
                            value={deliveryStockChina1Func()}
                            className='disabled'
                        />
                    </div>
                    <div className="modal_fourth_right_item">
                        <label>Курс ¥</label>
                        <Field 
                            type='number'
                            id='curs2'
                            name='curs2'
                            className={ formik.errors.curs2 && formik.touched.curs2 && 'input-error' }
                        />
                        {
                            formik.errors.curs2 && formik.touched.curs2 && 
                            <div className='error'>{ formik.errors.curs2 }</div>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
};


export default PayProduct;