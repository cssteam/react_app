import React from 'react';
import './Time.css'

const Time = () => {
    return (
        <div className='time'>
            <div className="time_previous time_item">
                Предыдущий
            </div>
            <div className="time_between">
                :
            </div>
            <div className="time_next time_item">
                Следующий
            </div>
        </div>
    );
};

export default Time;