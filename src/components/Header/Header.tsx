import React from 'react';
import { Link } from 'react-router-dom';
import './Header.css'

const Header = () => {
    return (
        <div className='header'>
            <div className="header_left">

            </div>
            <div className="header_center">
                <div className="header_center_item">
                    <Link to='/' className='link'>Накладные</Link>
                </div>
                <div className="header_center_item">
                    <Link to="/clients" className='link'>База Клиента</Link>
                </div>
                <div className="header_center_item">
                    <Link to="/calculation" className='link'>Вычисление</Link>
                </div>
            </div>
            <div className="header_right">
                <button>выйти</button>
            </div>
        </div>
    );
};

export default Header;