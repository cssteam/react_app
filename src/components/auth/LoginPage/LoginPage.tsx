import React from 'react';
import { Formik, Form, Field, FormikProps } from 'formik';
import * as Yup from 'yup'; 
import './LoginPage.css';

interface LoginType {
    email: string,
    password: string,
}

const LoginPage = () => {

    const Validate = Yup.object().shape({
        email: Yup.string().trim()
            .required('Пожалуйста заполните форму!'),
        password: Yup.string().trim()
            .required('Пожалуйста заполните форму!')
    })
    console.log('http://localhost:3000/api/auth/currentUser')

    return (
        <Formik
            initialValues={{
                email: '',
                password: ''
            }}
            validationSchema={Validate}
            onSubmit={(values) => {
                // console.log(values)
                alert(JSON.stringify(values, null, 2))
            }}
        >
            {(formik: FormikProps<LoginType>) => {
                return (
                    <Form>
                        <div className="login_background">
                            <div className="login_window">
                                <div className="login_window_item">
                                    <div className="login_window_item_title">
                                        Авторизация
                                    </div>
                                </div>
                                <div className="login_window_item">
                                    <div className="login_window_item_input">
                                        <label>Логин</label>
                                        <Field 
                                            type='text'
                                            id='email'
                                            name='email'
                                            placeholder='Введите адрес электронной почты'
                                            className={formik.errors.email && formik.touched.email && 'input-error'}>
                                        </Field>
                                        {
                                            formik.errors.email && formik.touched.email &&
                                            <div className='error'>{ formik.errors.email }</div>
                                        }
                                    </div>
                                </div>
                                <div className="login_window_item">
                                    <div className="login_window_item_input">
                                        <label>Пароль</label>
                                        <Field
                                            type='text'
                                            id='password'
                                            name='password'
                                            placeholder='Введите пароль'
                                            className={formik.errors.password && formik.touched.password && 'input-error'}>
                                        </Field>
                                        {
                                            formik.errors.password && formik.touched.password &&
                                            <div className='error'>{ formik.errors.password }</div>
                                        }
                                    </div>
                                </div>
                                <div className="login_window_item">
                                    <div className="login_window_item_button">
                                        <button type='submit'>Войти</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Form>
                )
            }}
        </Formik>
    );
};

export default LoginPage;