import { combineReducers } from "redux";
import { AuthReducer } from "./AuthReducer/AuthReducer";


export const RootReducer = combineReducers({
    auth: AuthReducer
})

export type AppState = ReturnType<typeof RootReducer>