import { Actions } from "../../Actions/Actions"

export interface Fetch_Current_User {
    type: Actions.FETCH_CURRENT_USER,
    payload: any
}

export type ActionType = Fetch_Current_User

export type AuthStateType = {
    currentUser: null | any
}

export const initialState: AuthStateType = {
    currentUser: null
}

export const AuthReducer = (state = initialState, action: ActionType) => {
    switch (action.type) {
        case Actions.FETCH_CURRENT_USER:
            return {...state, currentUser: action.payload}
        default: 
            return state
    }
}