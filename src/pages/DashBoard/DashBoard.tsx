import React from 'react';
import { Outlet } from 'react-router-dom';
import Header from '../../components/Header/Header';

const DashBoard = () => {
    return (
        <div className='dashBoard'>
            <div className="dashBoard_header">
                <Header />
            </div>
            <div className="dashBoard_outlet">
                <Outlet />
            </div>
        </div>
    );
};

export default DashBoard;