import React, { useState } from 'react';
import Modal from '../../components/CreateForm/CreateForm';
import Time from '../../components/Time/Time';
import './Documents.css';

const Ducuments = () => {
    const [modal, setModal] = useState(false)

    return (
        <div className='documents'>
            <div className="documents_first">
                <div className="documents_first_item">

                </div>
                <div className="documents_first_item">
                    <button onClick={() => setModal(true)}>создать накладной</button>
                </div>
            </div>
            <div className="documents_second">
                <div className="documents_second_item">
                    <input type="text" placeholder='Поиск...' /> 
                </div>
            </div>
            <div className="documents_third">
                <div className="documents_third_item">

                </div>
                <div className="documents_third_item">
                    <Time />
                </div>
            </div>
            <div className="documents_fourth">
                <div className="documents_fourth_item">
                    <div className="documents_fourth_item_list">
                        <div className="documents_fourth_item_list_item">
                            <div className="documents_fourth_item_list_title">
                                Название
                            </div>
                        </div>
                        <div className="documents_fourth_item_list_item">
                            <div className="documents_fourth_item_list_code-client">
                                Код Клиента
                            </div>
                        </div>
                        <div className="documents_fourth_item_list_item">
                            <div className="documents_fourth_item_list_date-create">
                                Дата Создания
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           { modal && <Modal onClose={() => setModal(false)}/> } 
        </div>
    );
};

export default Ducuments;