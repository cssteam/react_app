import React from 'react';
import Time from '../../components/Time/Time';
import './Clients.css'

const Clients = () => {
    return (
        <div className='clients'>
            <div className="clients_first">
                <div className="clients_first_item">
                    <input type="text" placeholder='Поиск...'/>
                </div>
            </div>
            <div className="clients_second">
                <div className="clients_second_item">

                </div>
                <div className="clients_second_item">
                    <Time />
                </div>
            </div>
            <div className="clients_third">
                <div className="clients_third_item">
                    <div className="clients_third_item_list">
                        <div className="clients_third_item_list_item">
                            <div className="clients_third_item_list_item_name">
                                ФИО
                            </div>
                        </div>
                        <div className="clients_third_item_list_item">
                            <div className="clients_third_item_list_item_code-client">
                                Код клиента
                            </div>
                        </div>
                        <div className="clients_third_item_list_item">
                            <div className="clients_third_item_list_item_password">
                                Паспорт
                            </div>
                        </div>
                        <div className="clients_third_item_list_item">
                            <div className="clients_third_item_list_item_phone-number">
                                Телефонный номер
                            </div>
                        </div>
                        <div className="clients_third_item_list_item">
                            <div className="clients_third_item_list_item_type-freight">
                                Вид фрахта
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Clients;