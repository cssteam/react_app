import React from 'react';
import './Data.css'

const Data = () => {
    return (
        <div className='data'>
            <div className="data_density">
                <div className="data_density_title title">
                    Данные о плотности
                </div>
                <div className="data_density_inputs-buttons">
                    <div className="data_density_inputs">
                        <div className="data_density_inputs_item">
                            <label htmlFor="from">От</label>
                            <input 
                                type='number' 
                                name='from'
                                id='from'
                                defaultValue={0}
                            />
                        </div>
                        <div className="data_density_inputs_item">
                            <label htmlFor="to">До</label>
                            <input 
                                type='number' 
                                name='from'
                                id='to'
                                defaultValue={0}
                            />
                        </div>
                        <div className="data_density_inputs_item">
                            <label htmlFor="price">Цена за кг</label>
                            <input 
                                type='number' 
                                name='price'
                                id='price'
                                defaultValue={0}
                            />
                        </div>
                        <div className="data_density_inputs_item">
                            <label>Тип</label>
                            <select>
                                <option value="1">Кг</option>
                                <option value="2">Обьем</option>
                            </select>
                        </div>
                    </div>
                    <div className="data_density_buttons">
                        <div className="data_density_buttons_item">
                            <button>добавить новое</button>
                        </div>
                        <div className="data_density_buttons_item">
                            <button>сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="data_pay">
                <div className="data_pay_title title">
                    Данные о оплаты
                </div>
                <div className="data_pay_inputs-buttons">
                    <div className="data_pay_inputs">
                        <div className="data_pay_inputs_item">
                            <label htmlFor="day">Дней</label>
                            <input 
                                type='number' 
                                name='day'
                                id='day'
                                defaultValue={0}
                            />
                        </div>
                    </div>
                    <div className="data_pay_buttons">
                        <div className="data_pay_buttons_item">
                            <button>добавить новое</button>
                        </div>
                        <div className="data_pay_buttons_item">
                            <button>сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="data_delivery">
                <div className="data_delivery_title title">
                    Данные о днях доставки
                </div>
                <div className="data_delivery_inputs-buttons">
                    <div className="data_delivery_inputs">
                        <div className="data_delivery_inputs_item">
                            <label htmlFor="day-delivery">Дней</label>
                            <input 
                                type='number' 
                                name='day-delivery'
                                id='day-delivery'
                                defaultValue={0}
                            />
                        </div>
                    </div>
                    <div className="data_delivery_buttons">
                        <div className="data_delivery_buttons_item">
                            <button>добавить новое</button>
                        </div>
                        <div className="data_delivery_buttons_item">
                            <button>сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Data;