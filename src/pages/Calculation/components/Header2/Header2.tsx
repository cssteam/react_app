import React from 'react';
import './Header2.css'
import { Link } from 'react-router-dom';

const Header2 = () => {
    return (
        <div className='header2'>
            <div className="header_item">
                <Link to='' className='link'>Экспресс</Link>
            </div>
            <div className="header_item">
                <Link to="avia" className='link'>Авиа</Link>
            </div>
            <div className="header_item">
                <Link to="slowAvia" className='link'>Медленная Авиа</Link>
            </div>
            <div className="header_item">
                <Link to="train" className='link'>Поезд</Link>
            </div>
            <div className="header_item">
                <Link to="train2" className='link'>Поезд(Один пояс один путь)</Link>
            </div>
            <div className="header_item">
                <Link to="trainCloth" className='link'>Поезд(Одежда)</Link>
            </div>
            <div className="header_item">
                <Link to="fastAuto" className='link'>Быстрый авто</Link>
            </div>
            <div className="header_item">
                <Link to="fastAuto2" className='link'>Быстрый авто(по белой)</Link>
            </div>
            <div className="header_item">
                <Link to="fastAutoCloth" className='link'>Быстрый авто(Одежда)</Link>
            </div>
            <div className="header_item">
                <Link to="slowAutoCloth" className='link'>Медленный авто(Одежда)</Link>
            </div>
            <div className="header_item">
                <Link to="slowAuto" className='link'>Медленный Авто</Link>
            </div>
        </div>
    );
};

export default Header2;