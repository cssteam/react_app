import React from 'react';
import { Outlet } from 'react-router-dom';
import './Calculation.css'
import Data from './components/Data/Data';
import Header2 from './components/Header2/Header2';

const Calculation = () => {
    return (
        <div className='calculation'>
            <Header2 />
            <Outlet />
        </div>
    );
};

export default Calculation;